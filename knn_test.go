package knn_test

import (
	"fmt"
	"testing"

	"gitlab.com/xeptore-group/university/data-minings/knn"

	. "github.com/smartystreets/goconvey/convey"
)

func TestNew(t *testing.T) {
	Convey("Testing New", t, func() {
		Convey("It should panic when k == 0", func() {
			So(func() {
				knn.New(0)
			}, ShouldPanic)
		})
	})
}

func TestTrain(t *testing.T) {
	knn := knn.New(1)
	Convey("Given an invalid sample training set", t, func() {
		trainingSet := [][]float64{
			[]float64{0.5250556842, 0.7118644331, 0.7939593369, 0.1805211422, 0.7530593749},
			[]float64{0.1924525513, 0.5497628932, 0.0736628504, 0.7610355148, 0.0130582649},
			[]float64{0.3971301124, 0.1374880353, 0.4062315399, 0.1351260608, 0.0770507924, 0.8367663656},
		}

		labels := []string{"1", "0", "0"}

		Convey("It should panic on training set", func() {
			So(func() {
				knn.Train(trainingSet, labels)
			}, ShouldPanic)
		})
	})

	Convey("Given an valid sample training set", t, func() {
		trainingSet := [][]float64{
			[]float64{0.5250556842, 0.7118644331, 0.7939593369, 0.1805211422, 0.7530593749},
			[]float64{0.1924525513, 0.5497628932, 0.0736628504, 0.7610355148, 0.0130582649},
			[]float64{0.3971301124, 0.1374880353, 0.4062315399, 0.1351260608, 0.0770507924},
		}

		Convey("And feeding with invalid labels", func() {
			labels := []string{"1", "0", "0", "1"}

			Convey("It should panic on labels set", func() {
				So(func() {
					knn.Train(trainingSet, labels)
				}, ShouldPanic)
			})
		})
	})

	Convey("Given an valid sample training set", t, func() {
		trainingSet := [][]float64{
			[]float64{0.5250556842, 0.7118644331, 0.7939593369, 0.1805211422, 0.7530593749},
			[]float64{0.1924525513, 0.5497628932, 0.0736628504, 0.7610355148, 0.0130582649},
			[]float64{0.3971301124, 0.1374880353, 0.4062315399, 0.1351260608, 0.0770507924},
		}

		Convey("And feeding with valid labels", func() {
			labels := []string{"1", "0", "0"}

			Convey("It should not panic", func() {
				So(func() {
					knn.Train(trainingSet, labels)
				}, ShouldNotPanic)
			})
		})
	})
}

func TestPredictSample(t *testing.T) {
	Convey("Training knn with a training set and labels", t, func() {
		trainingSet := [][]float64{
			[]float64{0.5250556842, 0.7118644331, 0.7939593369, 0.1805211422, 0.7530593749},
			[]float64{0.5250556624, 0.7118644324, 0.7939593243, 0.1805211246, 0.7530593315},
			[]float64{0.1924525513, 0.5497628932, 0.0736628504, 0.7610355148, 0.0130582649},
			[]float64{0.3971301124, 0.1374880353, 0.4062315399, 0.1351260608, 0.0770507924},
		}

		labels := []string{"1", "1", "0", "1"}

		Convey("It should panic when sample has different dimensions", func() {
			classifier := knn.New(1)
			classifier.Train(trainingSet, labels)
			sample := []float64{0.1924525243, 0.5497628354, 0.0733518504, 0.761365148}

			So(func() {
				classifier.PredictSample(sample)
			}, ShouldPanic)
		})

		Convey("It must predict a sample with label 0", func() {
			classifier := knn.New(1)
			classifier.Train(trainingSet, labels)
			sample := []float64{0.1924525243, 0.5497628354, 0.0733518504, 0.761365148, 0.0139682649}
			label := classifier.PredictSample(sample)

			So(label, ShouldEqual, "0")
		})

		Convey("It must predict a sample with label 1", func() {
			classifier := knn.New(2)
			classifier.Train(trainingSet, labels)
			sample := []float64{0.5250556842, 0.7118644331, 0.7939593369, 0.1805211422, 0.7530593749}
			label := classifier.PredictSample(sample)

			So(label, ShouldEqual, "1")
		})
	})
}

func TestPredictSamples(t *testing.T) {
	knn := knn.New(1)
	Convey("Training knn with a training set and labels", t, func() {
		trainingSet := [][]float64{
			[]float64{0.5250556842, 0.7118644331, 0.7939593369, 0.1805211422, 0.7530593749},
			[]float64{0.1924525513, 0.5497628932, 0.0736628504, 0.7610355148, 0.0130582649},
			[]float64{0.3971301124, 0.1374880353, 0.4062315399, 0.1351260608, 0.0770507924},
		}

		labels := []string{"0", "1", "0"}

		knn.Train(trainingSet, labels)

		Convey("It must predict samples with correct labels", func() {
			samples := [][]float64{
				[]float64{0.1924525243, 0.5497628354, 0.0733518504, 0.761365148, 0.0139682649},
				[]float64{0.5250556842, 0.7118644331, 0.7939593369, 0.1805211422, 0.7530593749},
			}
			labels := knn.PredictSamples(samples)

			So(labels, ShouldResemble, []string{"1", "0"})
		})
	})
}

func TestScore(t *testing.T) {
	knn := knn.New(1)
	Convey("Training knn with a training set and labels", t, func() {
		trainingSet := [][]float64{
			[]float64{0.5250556842, 0.7118644331, 0.7939593369, 0.1805211422, 0.7530593749},
			[]float64{0.1924525513, 0.5497628932, 0.0736628504, 0.7610355148, 0.0130582649},
			[]float64{0.3971301124, 0.1374880353, 0.4062315399, 0.1351260608, 0.0770507924},
		}

		labels := []string{"0", "1", "0"}

		knn.Train(trainingSet, labels)

		Convey("It must score 1.0", func() {
			samples := [][]float64{
				[]float64{0.1924525243, 0.5497628354, 0.0733518504, 0.761365148, 0.0139682649},
				[]float64{0.5250556842, 0.7118644331, 0.7939593369, 0.1805211422, 0.7530593749},
			}
			score := knn.Score(samples, []string{"1", "0"})

			So(score, ShouldEqual, 1.0)
		})

		Convey("It must score 0.5 with 50% correct-predicted labels", func() {
			samples := [][]float64{
				[]float64{0.1924525243, 0.5497628354, 0.0733518504, 0.761365148, 0.0139682649},
				[]float64{0.5250556842, 0.7118644331, 0.7939593369, 0.1805211422, 0.7530593749},
			}
			score := knn.Score(samples, []string{"1", "1"})

			So(score, ShouldEqual, 0.5)
		})

		Convey("It must score 0.333 with 1 out of three correct-predicted labels", func() {
			samples := [][]float64{
				[]float64{0.5250556842, 0.7118644331, 0.7939593369, 0.1805211422, 0.7530593749},
				[]float64{0.1924525513, 0.5497628932, 0.0736628504, 0.7610355148, 0.0130582649},
				[]float64{0.3971301124, 0.1374880353, 0.4062315399, 0.1351260608, 0.0770507924},
			}
			score := knn.Score(samples, []string{"1", "1", "1"})

			So(fmt.Sprintf("%.3f", score), ShouldEqual, "0.333")
		})

		Convey("It must score 0.5 with all wrong-predicted labels", func() {
			samples := [][]float64{
				[]float64{0.1924525243, 0.5497628354, 0.0733518504, 0.761365148, 0.0139682649},
				[]float64{0.5250556842, 0.7118644331, 0.7939593369, 0.1805211422, 0.7530593749},
			}
			score := knn.Score(samples, []string{"0", "1"})

			So(score, ShouldEqual, 0.0)
		})
	})
}
