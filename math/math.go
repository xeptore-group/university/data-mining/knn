package math

import (
	"math"
)

// Distance returns Euclidean Distance of two points with same dimensions.
func Distance(p1, p2 []float64) float64 {
	if len(p1) != len(p2) {
		panic("points must have same dimensions")
	}

	d := 0.0

	for i := 0; i < len(p1); i++ {
		d += math.Pow(p1[i]-p2[i], 2)
	}

	return math.Sqrt(d)
}
