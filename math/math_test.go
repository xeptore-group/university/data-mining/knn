package math

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestDistance(t *testing.T) {
	Convey("Given some valid points", t, func() {

		results := map[float64]float64{
			1.0: Distance(
				[]float64{1.0, 0.0},
				[]float64{0.0, 0.0},
			),
			0.0: Distance(
				[]float64{3.0, 0.0},
				[]float64{3.0, 0.0},
			),
			5.0: Distance(
				[]float64{0.0, 0.0},
				[]float64{3.0, 4.0},
			),
		}

		Convey("Calculated distance should be equal to expected", func() {
			for expected, got := range results {
				if got != expected {
					So(expected, ShouldEqual, got)
				}
			}
		})
	})

	Convey("Given an invalid points pair", t, func() {
		points := [][]float64{
			[]float64{1.0, 0.0},
			[]float64{0.0, 0.0, 1.2},
		}

		Convey("It must panic", func() {
			So(func() {
				Distance(points[0], points[1])
			}, ShouldPanic)
		})
	})
}
