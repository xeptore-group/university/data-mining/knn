package knn

import (
	"fmt"
	"sort"

	"gitlab.com/xeptore-group/university/data-minings/knn/math"
)

// KNearestNeighbor represents KNearestNeighbors instace structure.
type KNearestNeighbor struct {
	k           uint
	dimensions  uint
	trainTuples []trainTuple
}

type trainTuple struct {
	point []float64
	label string
}

// New returns a new instance of KNearestNeighbor.
func New(k uint) KNearestNeighbor {
	if k == 0 {
		panic("k must be > 0")
	}
	return KNearestNeighbor{k: k}
}

// Train trains the KNN model receiving training set data.
func (knn *KNearestNeighbor) Train(set [][]float64, labels []string) {
	if len(set) != len(labels) {
		panic("training set and labels must have equal length")
	}

	rowLength := len(set[0])
	for i, row := range set[1:] {
		if len(row) != rowLength {
			panic(fmt.Errorf("train set %d differs in length", i))
		}
	}

	knn.dimensions = uint(rowLength)

	knn.trainTuples = make([]trainTuple, len(set))
	for i := 0; i < len(set); i++ {
		knn.trainTuples[i] = trainTuple{
			label: labels[i],
			point: set[i],
		}
	}
}

// PredictSample predicts the class label of a sample
func (knn *KNearestNeighbor) PredictSample(sample []float64) string {
	if uint(len(sample)) != knn.dimensions {
		panic("test sample differs in dimensions")
	}

	distances := make([]sampleDistance, len(knn.trainTuples))
	for i := 0; i < len(knn.trainTuples); i++ {
		d := math.Distance(sample, knn.trainTuples[i].point)
		distances[i] = sampleDistance{
			distance:   d,
			tupleIndex: uint(i),
		}
	}

	sort.Sort(byDistance(distances))

	topRows := distances[:knn.k]

	frequentLabels := make(map[string]uint, len(topRows))
	for _, row := range topRows {
		label := knn.trainTuples[row.tupleIndex].label
		if _, ok := frequentLabels[label]; ok {
			frequentLabels[label]++
		} else {
			frequentLabels[label] = 1
		}
	}

	mostFrequestLabel := struct {
		label     string
		frequency uint
	}{
		label:     "",
		frequency: 0,
	}
	for label, frequency := range frequentLabels {
		if frequency > mostFrequestLabel.frequency {
			mostFrequestLabel.frequency = frequency
			mostFrequestLabel.label = label
		}
	}

	return mostFrequestLabel.label
}

// PredictSamples returns predicted labels for samples.
func (knn *KNearestNeighbor) PredictSamples(samples [][]float64) []string {
	out := make([]string, len(samples))
	for i := 0; i < len(samples); i++ {
		out[i] = knn.PredictSample(samples[i])
	}

	return out
}

// Score returns mean accuracy on given tests and labels.
func (knn *KNearestNeighbor) Score(samples [][]float64, labels []string) float64 {
	predictedLabels := knn.PredictSamples(samples)

	scoresSum := 0
	for i, predictedLabel := range predictedLabels {
		if predictedLabel == labels[i] {
			scoresSum++
		}
	}

	return float64(scoresSum) / float64(len(labels))
}
