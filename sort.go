package knn

type sampleDistance struct {
	distance   float64
	tupleIndex uint
}

type byDistance []sampleDistance

func (d byDistance) Len() int           { return len(d) }
func (d byDistance) Less(i, j int) bool { return d[i].distance < d[j].distance }
func (d byDistance) Swap(i, j int)      { d[i], d[j] = d[j], d[i] }
